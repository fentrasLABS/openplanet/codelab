# Codelab <sup><sub>Trackmania ManiaPlanet Turbo</sub></sup>
Edit your code right from the *Openplanet* interface.

## Features
* Multiple windows
* Multiple tabs

## Screenshots
![](_git/1.png)

## Credits
- Project icon by [Fork Awesome](https://forkaweso.me)
- Interface code from [Plugin Manager by Miss](https://github.com/openplanet-nl/plugin-manager)
- Layout inspired by [Visual Studio Code](https://code.visualstudio.com/)