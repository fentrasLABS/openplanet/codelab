class Plugin : Info
{
    string GetLabel() { return "Codelab"; }
    string GetIcon() { return Icons::Flask; }
    string GetColor() { return "\\$33f"; }
}

Plugin plugin;
array<Editor@> editors;

void RenderMenuMain()
{
    if (UI::BeginMenu(plugin.GetTitle())) {
        UI::PushStyleColor(UI::Col::Text, vec4(1, 1, 1, 1));
        if (UI::MenuItem(Icons::Code + " Editor", "Ctrl+Shift+N"))
            Editor::New(editors);
        UI::PopStyleColor();
        UI::EndMenu();
    }
    if (editors.Length > 0) {
        for (uint i = 0; i < editors.Length; i++) {
            auto editor = editors[i];
            // Use tab name instead of global name (editor.selectTab.GetLabel())
            if (UI::MenuItem(editor.GetNameIcon(), "", editor.visible)) {
                editor.visible = !editor.visible;
            }
        }
    }
}

void RenderInterface()
{
    if (editors.Length > 0) {
        for (uint i = 0; i < editors.Length; i++) {
            auto editor = editors[i];
            editor.Render();
            if (!editor.open)
                editors.RemoveAt(i--);
        }
    }
}