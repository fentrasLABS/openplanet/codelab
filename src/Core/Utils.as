namespace Utils
{
    uint CalculateID(array<Editor@> list)
    {
        uint length = list.Length;
        for (uint i = 0; i < length; i++)
            if (i != list[i].id)
                return i;
        return length;
    }
}