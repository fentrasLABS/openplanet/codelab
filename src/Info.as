class Info
{
    string GetLabel() { return ""; }
    string GetIcon() { return Icons::Heartbeat; }
    string GetColor() { return "\\$z"; }
    string GetTitle() { return GetIcon() + " " + GetLabel(); }
    string GetTitleColored() { return GetColor() + GetTitle(); }
}
