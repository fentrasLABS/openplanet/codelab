namespace Editor
{
	void New(array<Editor@>@ list)
	{
		uint id = Utils::CalculateID(list);
        list.InsertAt(id, Editor(id, list));
	}
}

class Editor : Info
{
	string GetLabel() override { return "Codelab Editor"; }
    string GetIcon() override { return Icons::Code; }
	string GetColor() override { return "\\$3ff"; }

	string GetID() { return id > 0 ? " #" + id : ""; }
	string GetTitleID() { return GetTitle() + GetID(); }
	string GetNameIcon() { return GetIcon() + " " + name; }
	string GetNameLabel() { return name + " — " + GetLabel(); }
	string GetNameLabelID() { return GetNameLabel() + GetID(); }
	string GetNameDefault() { return "Untitled"; }

	uint windowFlags = UI::WindowFlags::NoTitleBar + UI::WindowFlags::MenuBar;
	uint mainMenuHeight = 24;
	float windowDivider = 1.333f;

	bool open = true;
	bool visible = true;
	bool initial = true;
	bool changing = false;
	bool maximized = false;

	uint id;
	string name;
	array<Editor@>@ all;

	vec2 size;
	vec2 position;
	vec2 setSize;
	vec2 setPosition;

	array<Tab@> tabs; 
	Tab@ selectTab;
	Tab@ lastActiveTab;

	Editor(uint count, array<Editor@>@ list)
	{
		// Choose between welcomeTab, emptyTab and last session
		Tab@ welcomeTab = WelcomeTab();

		id = count;
		name = welcomeTab.GetLabel();
		@all = list;

		size = vec2(int(Draw::GetWidth() / windowDivider), int(Draw::GetHeight() / windowDivider));
		position = vec2(int(Draw::GetWidth() / 2), int(Draw::GetHeight() / 2));
		setSize = size;
		setPosition = position;
		AddTab(welcomeTab, true);
	}

	void Maximize(bool maximize = true)
	{
		if (maximize) {
			size = UI::GetWindowSize();
			position = UI::GetWindowPos();
			setSize = vec2(Draw::GetWidth(), Draw::GetHeight() - mainMenuHeight);
			setPosition = vec2(0, mainMenuHeight);
			maximized = true;
		} else {
			setSize = size;
			setPosition = position;
			maximized = false;
		}
		changing = true;
	}

	bool IsSizePosChanged(vec2 sizeCompare, vec2 posCompare)
	{
		if (sizeCompare.x != setSize.x)
			return true;
		if (sizeCompare.y != setSize.y)
			return true;
		if (posCompare.x != setPosition.x)
			return true;
		if (posCompare.y != setPosition.y)
			return true;
		return false;
	}

	void AddTab(Tab@ tab, bool select = false)
	{
		tabs.InsertLast(tab);
		if (select) {
			@selectTab = tab;
		}
	}

	void RenderTabContents(Tab@ tab)
	{
		UI::BeginChild("Tab");
		tab.Render();
		UI::EndChild();
		UI::EndTabItem();
	}
	
	void RenderTitleBarContents()
	{
		// Add double-click to maximize
		UI::BeginMenuBar();

		UI::Text(Icons::Code);
		if (UI::BeginMenu("File")) {
			if (UI::MenuItem("New Window")) {
				Editor::New(all);
			}
			if (UI::MenuItem("New Tab")) {
				AddTab(FileTab(), true);
			}
			UI::EndMenu();
		}
		UI::AlignTextToFramePadding();
		UI::Text(GetNameLabelID());
		UI::AlignTextToFramePadding();
		if (UI::MenuItem(Icons::WindowMinimize)) {
			visible = false;
		}
		if (!maximized) {
			if (UI::MenuItem(Icons::WindowMaximize)) {
				Maximize();
			}
		} else {
			if (UI::MenuItem(Icons::WindowRestore)) {
				Maximize(false);
			}
		}
		// Color hover and hover_pressed to red
		if (UI::MenuItem(Icons::WindowCloseO)) {
			open = false;
		}
		UI::EndMenuBar();
	}

	void Render()
	{
		if (!visible) {
			return;
		}

		if (initial) {
			UI::SetNextWindowSize(int(size.x), int(size.y));
			UI::SetNextWindowPos(int(position.x), int(position.y), UI::Cond::Appearing, 0.5f, 0.5f);
			initial = false;
		} else if (changing) {
			UI::SetNextWindowSize(int(setSize.x), int(setSize.y), UI::Cond::Always);
			UI::SetNextWindowPos(int(setPosition.x), int(setPosition.y), UI::Cond::Always);
			changing = false;
		}

		if (UI::Begin(GetTitleID(), visible, windowFlags)) {

			UI::PushID(GetID());

			RenderTitleBarContents();
			
			UI::BeginTabBar("Tabs");

			if (maximized && !changing && IsSizePosChanged(UI::GetWindowSize(), UI::GetWindowPos())) {
				Maximize(false);
			}

			for (uint i = 0; i < tabs.Length; i++) {
				auto tab = tabs[i];
				if (!tab.IsVisible()) {
					continue;
				}

				UI::PushID(tab);

				int flags = 0;
				if (tab is selectTab) {
					flags |= UI::TabItemFlags::SetSelected;
					@selectTab = null;
				}

				tab.PushTabStyle();

				if (tab.CanClose()) {
					bool tabOpen = true;
					if (UI::BeginTabItem(tab.GetLabel(), tabOpen, flags)) {
						@lastActiveTab = tab;
						RenderTabContents(tab);
					}
					if (!tabOpen) {
						tabs.RemoveAt(i--);
					}
				} else {
					if (UI::BeginTabItem(tab.GetLabel(), flags)) {
						@lastActiveTab = tab;
						name = tab.GetLabel();
						RenderTabContents(tab);
					}
				}

				tab.PopTabStyle();

				UI::PopID();
			}

			UI::EndTabBar();

			UI::PopID();
		}

		UI::End();
	}
}