class FileTab : Tab {
    string GetLabel() override { return "Untitled"; }

    int rand = Math::Rand(-65535, 65535);

    void Render() override
    {
        UI::Text("Untitled " + rand);
    }
}
