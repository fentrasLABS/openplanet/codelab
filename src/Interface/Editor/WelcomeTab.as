class WelcomeTab : Tab {
    string GetLabel() override { return "Get Started"; }

    int rand = Math::Rand(-65535, 65535);

    void Render() override
    {
        UI::Text("Welcome to Codelab! " + rand);
    }
}
